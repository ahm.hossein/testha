import styled from 'styled-components';

export const Container = styled.div`
    position: absolute;
    justify-content: center;
    width: 80%;
    height: 500px;

    @media (max-width: 1000px) {
        width: 100%;
    }
`;

export const Image = styled.img`
    height: 100%;
    width: 100%;
    object-fit: cover;
    border-radius: 100%/0 0 80px 80px;
    filter: brightness(60%);
`;
