import { ComponentProps } from 'react';
import { Container, Image } from './styles';

type Props = ComponentProps<typeof Image>;

export default function BackgroundImage({ ...restProps }: Props) {
    return (
        <Container>
            <Image {...restProps} />
        </Container>
    );
}
