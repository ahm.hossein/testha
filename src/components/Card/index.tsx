import { ComponentProps } from "react";
import { GeneralProps, IconType, LogoType } from "../../types";
import {
    Container,
    Title,
    RatingContainer,
    Rating,
    Row,
    Logo,
    Item,
    ItemTitle,
    ItemDesc,
    Caption,
    Icon,
} from './styles';

type IconProps = {
    name: IconType;
}

type RatingProps = {
    rate: string;
}

type LogoProps = {
    logo: LogoType;
}


type ItemTitleProps = ComponentProps<typeof ItemTitle> & GeneralProps;

export default function Card({ children, ...restProps }: GeneralProps) {
    return <Container {...restProps}>{children}</Container>;
}

Card.Item = function CardItem({ children, ...restProps }: GeneralProps) {
    return <Item {...restProps}>{children}</Item>
}

Card.Title = function CardTitle({ children, ...restProps }: GeneralProps) {
    return <Title {...restProps}>{children}</Title>;
}

Card.Row = function CardRow({ children, ...restProps }: GeneralProps) {
    return <Row {...restProps}>{children}</Row>;
}

Card.Rating = function CardRating({ rate, ...restProps }: RatingProps) {
    return (
        <RatingContainer>
            <Rating isBold {...restProps}>{rate}</Rating>
            <Rating> / 5</Rating>
        </RatingContainer>
    );
}

Card.Logo = function CardLogo({ logo, ...restProps }: LogoProps) {
    return <Logo src={`/icons/${logo}.svg`} {...restProps} />;
}

Card.ItemTitle = function CardItemTitle({ children, ...restProps }: ItemTitleProps) {
    return <ItemTitle {...restProps}>{children}</ItemTitle>;
}

Card.Desc = function CardItemDesc({ children, ...restProps }: GeneralProps) {
    return <ItemDesc {...restProps}>{children}</ItemDesc>
}

Card.Caption = function CardCaption({ children, ...restProps }: GeneralProps) {
    return <Caption {...restProps}>{children}</Caption>;
}

Card.Pros = function CardPros({ ...restProps }) {
    return <StatusIcon name='thumb-up' {...restProps} />;
}

Card.Cons = function CardCons({ ...restProps }) {
    return <StatusIcon name='thumb-down' {...restProps} />;
}

function StatusIcon({ name, ...restProps }: IconProps) {
    return <Icon src={`/icons/${name}.svg`} {...restProps} />
}

