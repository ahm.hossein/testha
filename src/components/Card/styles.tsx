import styled from 'styled-components';

type ItemTitleProps = {
    large?: boolean;
}

type RatingTextProps = {
    isBold?: boolean;
}

export const Container = styled.div`
    border-radius: 6px;
    box-shadow: 0 0 2px 1px rgba(0, 0, 0, 0.2);
    max-width: 100%;
    background: white;
    padding: 24px;
    margin-bottom: 24px;
`;

export const Title = styled.h1`
    color: #193854;
`;

export const Row = styled.div`
    display: flex;
`;

export const RatingContainer = styled.div`
    padding: 6.5px 10px;
    background: #0276DB;
    border-radius: 8px;
    display: inline-block;
`;

export const Rating = styled.span`
    color: white;
    font-weight: ${({ isBold }: RatingTextProps) => isBold ? 'bold' : 'normal'};
`;

export const Logo = styled.img`
    width: 70px;
    margin: 0 12px;
`;

export const Item = styled.div`
    padding: 24px 0px;
    border-bottom: 1px solid #E1E4E7;
    
    &:last-child {
        border: 0;
    }
`;

export const ItemTitle = styled.p`
    color: #173753;
    font-weight: bold;
    font-size: ${({ large }: ItemTitleProps) => large ? '17px' : '14px'};
    margin-bottom: ${({ large }: ItemTitleProps) => !large ? '8px' : 'auto'};
`;

export const ItemDesc = styled.p`
    color: #173753;
    font-size: 14px;
    line-height: 1.7em;
`;

export const Caption = styled.p`
    font-size: 12px;
    color: #677B8F;
    margin: 0;
`;

export const Icon = styled.img`
    width: 20px;
    margin-right: 8px;
    margin-bottom: -4px;
`;
