import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    position: relative;
    width: 55%;
    padding-top: 24px;

    @media (max-width: 1000px) {
        width: 80%;
        padding: 24px;
    }
`;

export const Title = styled.h1`
    color: white;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
    margin-top: 0;
`;

export const Caption = styled.p`
    color: white;
    text-shadow: 1px 1px rgba(0, 0, 0, 0.3);
`;
