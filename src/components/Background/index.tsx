import { GeneralProps } from '../../types';
import { Container, Title, Caption } from './styles';

export default function Background({ children, ...restProps }: GeneralProps) {
    return <Container {...restProps}>{children}</Container>;
}

Background.Title = function BackgroundTitle({ children, ...restProps }: GeneralProps) {
    return <Title {...restProps}>{children}</Title>;
}

Background.Caption = function BackgroundCaption({ children, ...restProps }: GeneralProps) {
    return <Caption {...restProps}>{children}</Caption>
}
