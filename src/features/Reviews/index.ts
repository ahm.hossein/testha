import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getReviews } from '../../lib/Api';
import { AppThunk } from '../../store';
import { Review } from '../../types';

interface ReviewState {
    items: Review[];
    totalReviews: number;
    isLoading: boolean;
    error: string | null;
    page: number;
    isLastPage: boolean;
}

const initialState: ReviewState = {
    items: [],
    totalReviews: 0,
    isLoading: false,
    error: null,
    page: 1,
    isLastPage: false,
};

const startLoading = (state: ReviewState) => {
    state.isLoading = true
};
const fetchFailure = (state: ReviewState, action: PayloadAction<string>) => {
    state.isLoading = false;
    state.error = action.payload;
}
const fetchSuccess = (state: ReviewState, { payload }: PayloadAction<Review[]>) => {
    state.isLoading = false;
    state.error = null;
    if (payload.length === 0) {
        state.isLastPage = true;
    } else {
        state.items.push(...payload);
        state.totalReviews += payload.length;
    }
}
const loadNextPage = (state: ReviewState) => {
    if (!state.isLastPage) {
        state.page += 1;
    }
}

const reviews = createSlice({
    name: 'reviews',
    initialState,
    reducers: {
        fetchReviewStart: startLoading,
        fetchReviewsFailure: fetchFailure,
        fetchReviewsSuccess: fetchSuccess,
        updatePage: loadNextPage,
    }
})

export const {
    fetchReviewStart,
    fetchReviewsFailure,
    fetchReviewsSuccess,
    updatePage,
} = reviews.actions;

export default reviews.reducer;

export const fetchReviews = (page: number): AppThunk => async dispatch => {
    try {
        dispatch(fetchReviewStart());
        const reviews = await getReviews(page);
        dispatch(fetchReviewsSuccess(reviews));
    } catch (e) {
        dispatch(fetchReviewsFailure(e.toString()));
    }
}
