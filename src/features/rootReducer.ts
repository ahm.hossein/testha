import { combineReducers } from '@reduxjs/toolkit';
import reviewsReducer from './Reviews';

const rootReducer = combineReducers({
    reviews: reviewsReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
