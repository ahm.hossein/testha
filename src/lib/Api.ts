import axios from 'axios';
import { Review } from '../types';


export async function getReviews(page = 1): Promise<Review[]> {
    const url = `https://interview-task-api.bookiply.io/reviews?_page=${page}`;
    try {
        const reviews = await axios.get<Review[]>(url);
        return reviews.data;
    } catch (e) {
        throw e;
    }
}
