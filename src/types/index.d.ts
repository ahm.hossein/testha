import { ReactNode } from "react";

declare type GeneralProps = {
    children: ReactNode;
}


declare type LogoType = 'AIRBNB' | 'BOOKINGCOM' | 'HOLIDU';

declare type IconType = 'thumb-down' | 'thumb-up';

declare type Review = {
    headline: string;
    comment: string;
    author: string;
    positiveFeedback: string | null;
    negativeFeedback: string | null;
    score: number;
    channel: LogoType;
    publishedAt: string;
}
