import { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import moment from 'moment';
import { RootState } from '../features/rootReducer';
import { fetchReviews, updatePage } from '../features/Reviews';
import { Card } from '../components';

export default function ReviewsContainer() {
    const dispatch = useDispatch();
    const {
        error,
        isLoading,
        page,
        items,
        totalReviews,
    } = useSelector((state: RootState) => state.reviews);

    const loader = useRef<HTMLDivElement>(null);
    let isFirst = true;

    useEffect(() => {
        const options = {
            root: null,
            rootMargin: "100px",
            threshold: 1.0
        };
        const observer = new IntersectionObserver(handleObserver, options);
        if (loader.current) {
            observer.observe(loader.current)
        }
    }, []);

    useEffect(() => {
        dispatch(fetchReviews(page));
    }, [dispatch, page]);

    const handleObserver = (entities: IntersectionObserverEntry[]) => {
        const target = entities[0];
        if (target.isIntersecting && !isFirst) {
            dispatch(updatePage());
        }
        isFirst = false;
    }

    if (error) {
        return <Card>Error: {error}</Card>;
    }

    return (
        <>
            <Card>
                {!isLoading && <Card.Title>{totalReviews} Reviews</Card.Title>}
                {
                    items.map((review, index) => {
                        return (
                            <Card.Item key={index.toString()}>
                                <Card.Row>
                                    <Card.Rating rate={review.score.toString()} />
                                    <Card.Logo logo={review.channel} />
                                </Card.Row>
                                <Card.ItemTitle large>{review.headline}</Card.ItemTitle>
                                <Card.Desc>{review.comment}</Card.Desc>
                                {review.positiveFeedback && <Card.Desc><Card.Pros />{review.positiveFeedback}</Card.Desc>}
                                {review.negativeFeedback && <Card.Desc><Card.Cons />{review.negativeFeedback}</Card.Desc>}
                                <Card.ItemTitle>{review.author}</Card.ItemTitle>
                                <Card.Caption>Reviewed {moment(review.publishedAt).format('DD MMMM YYYY')}</Card.Caption>
                            </Card.Item>
                        );
                    })
                }
                {isLoading && <p>Loading...</p>}
            </Card>
            <div ref={loader} />
        </>
    );
}
