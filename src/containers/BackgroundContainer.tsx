import { GeneralProps } from "../types";
import { Background, BackgroundImage } from '../components';

export default function BackgroundContainer({ children }: GeneralProps) {
    return (
        <>
            <BackgroundImage src='/images/background.png' />
            <Background>
                <Background.Caption>ID: 091021</Background.Caption>
                <Background.Title>La Casa de las Flores</Background.Title>
                {children}
            </Background>
        </>
    );
}