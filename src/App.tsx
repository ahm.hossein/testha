import { BackgroundContainer } from './containers';
import { Home } from './pages';

function App() {
  return (
    <BackgroundContainer>
      <Home />
    </BackgroundContainer>
  );
}

export default App;
